package client

import (
	"embed"
	"encoding/json"
	"log"
	"net"
	"strconv"

	"gopkg.in/yaml.v2"
)

//go:embed config.yaml
var configData embed.FS
var conn net.Conn

type ServerConfig struct {
	Active  bool   `yaml:"active"`
	Address string `yaml:"address"`
	Port    int    `yaml:"port"`
}

type Message struct {
	Hostname string `json:"hostname"`
	Msg      string `json:"msg"`
}

// read and return the address of the server based of the configuration file
func (s ServerConfig) ServerAddress() string {
	return string(s.Address + ":" + strconv.Itoa(s.Port))
}

func NewServer() ServerConfig {
	data, err := configData.ReadFile("config.yaml")
	if err != nil {
		log.Fatalln("Error: ", err)
	}
	var s ServerConfig
	err = yaml.Unmarshal(data, &s)
	if err != nil {
		log.Fatalln("Error: ", err)
	}

	return s
}

func (s ServerConfig) InitConnection() error {
	var err error
	conn, err = net.Dial("udp", s.ServerAddress())
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (s ServerConfig) Send(hostname string, message string) {
	var msg Message
	msg.Hostname = hostname
	msg.Msg = message

	jsonMsg, err := json.Marshal(msg)
	if err != nil {
		log.Println("Err when marshalling message: " + err.Error())
	}
	_, err = conn.Write([]byte(jsonMsg))
	if err != nil {
		log.Println(err.Error())
	}
}
